#pragma once
#include "vector.hpp"
__device__ inline Vector3 UniformSampleOnHemisphere(JINGDU u1,
	JINGDU u2) {
	// u1 := cos_theta
	const JINGDU sin_theta = std::sqrt(fmax(0.0, 1.0 - u1 * u1));
	const JINGDU phi = 2.0 * g_pi * u2;
	return {
		std::cos(phi) * sin_theta,
		std::sin(phi) * sin_theta,
		u1
	};
}

__device__ inline Vector3 CosineWeightedSampleOnHemisphere(JINGDU u1,
	JINGDU u2) {
	const JINGDU cos_theta = sqrt(1.0 - u1);
	const JINGDU sin_theta = sqrt(u1);
	const JINGDU phi = 2.0f * g_pi * u2;
	return {
		std::cos(phi) * sin_theta,
		std::sin(phi) * sin_theta,
		cos_theta
	};
}
