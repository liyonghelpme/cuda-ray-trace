#pragma once
#include "device_launch_parameters.h"
#include <cmath>
#include <limits>
#include <cstdint>
#include <algorithm>
#define JINGDU float

constexpr JINGDU g_pi = 3.1415926535897932384f;

__host__ __device__ inline JINGDU Clamp(JINGDU v, JINGDU low = 0.0f, JINGDU high = 1.0f) noexcept {
	return fmin(fmax(v, low), high);
}

inline std::uint8_t ToByte(JINGDU color, JINGDU gamma=2.2f) noexcept {
	JINGDU gcolor = std::pow(color, 1.0f / gamma);
	return static_cast<std::uint8_t>(Clamp(255.0f * gcolor, 0.0f, 255.0f));
}
