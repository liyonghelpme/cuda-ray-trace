#pragma once
#include "vector.hpp"
#include <cstdio>


inline void WritePPM(std::uint32_t w,
	std::uint32_t h,
	const Vector3* Ls,
	const char* fname = "cu-image.ppm") noexcept {

	FILE* fp;

	fopen_s(&fp, fname, "w");

	std::fprintf(fp, "P3\n%u %u\n%u\n", w, h, 255u);
	for (std::size_t i = 0; i < (std::size_t)w * (std::size_t) h; ++i) {
		std::fprintf(fp, "%u %u %u ",
			ToByte(Ls[i].x),
			ToByte(Ls[i].y),
			ToByte(Ls[i].z));
	}

	std::fclose(fp);
}
