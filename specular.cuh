#pragma once
#include "curand_kernel.h"
#include "vector.hpp"

__device__ inline JINGDU Reflectance0(JINGDU n1, JINGDU n2) {
	const JINGDU sqrt_R0 = (n1 - n2) / (n1 + n2);
	return sqrt_R0 * sqrt_R0;
}

__device__ inline JINGDU SchlickReflectance(JINGDU n1,
	JINGDU n2,
	JINGDU c) {

	const JINGDU R0 = Reflectance0(n1, n2);
	return R0 + (1.0 - R0) * c * c * c * c * c;
}

__device__ inline const Vector3 IdealSpecularReflect(const Vector3& d,
	const Vector3& n) {
	return d - 2.0 * n.Dot(d) * n;
}

__device__ inline const Vector3 IdealSpecularTransmit(const Vector3& d,
	const Vector3& n,
	JINGDU n_out,
	JINGDU n_in,
	JINGDU& pr,
	curandState* state) {

	//反射光朝向
	const Vector3 d_Re = IdealSpecularReflect(d, n);

	const bool out_to_in = (0.0 > n.Dot(d));
	const Vector3 nl = out_to_in ? n : -n;
	const JINGDU nn = out_to_in ? n_out / n_in : n_in / n_out;
	const JINGDU cos_theta = d.Dot(nl);
	const JINGDU cos2_phi = 1.0 - nn * nn * (1.0 - cos_theta * cos_theta);

	// Total Internal Reflection
	//全反射能量
	if (0.0 > cos2_phi) {
		pr = 1.0;
		return d_Re;
	}
	//透射方向
	const Vector3 d_Tr = Normalize(nn * d - nl * (nn * cos_theta + sqrt(cos2_phi)));
	const JINGDU c = 1.0 - (out_to_in ? -cos_theta : d_Tr.Dot(n));
	//透射能量分布
	const JINGDU Re = SchlickReflectance(n_out, n_in, c);
	//反射方向
	const JINGDU p_Re = 0.25 + 0.5 * Re;
	//走反射 能量
	if (curand_uniform(state) < p_Re) {
		pr = (Re / p_Re);
		return d_Re;
	}
	else { //走透射能量
		const JINGDU Tr = 1.0 - Re;
		const JINGDU p_Tr = 1.0 - p_Re;
		pr = (Tr / p_Tr);
		return d_Tr;
	}
}
