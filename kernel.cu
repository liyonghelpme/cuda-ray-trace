#include "imageio.hpp"
#include "sampling.cuh"
#include "specular.cuh"
#include "sphere.hpp"
#include <chrono>
#include <iostream>
using namespace std::chrono;

#include "cuda_tools.hpp"
//入射折射率 
#define REFRACTIVE_INDEX_OUT 1.0
#define REFRACTIVE_INDEX_IN  1.5
#define Vec Vector3
//数值范围
#define DIFF  Reflection_t::Diffuse
#define SPEC  Reflection_t::Specular
#define REFR  Reflection_t::Refractive
const Sphere g_spheres[] = {
	Sphere(1e4,  Vector3(1e4 + 1, 40.8, 81.6),   Vector3(),   Vector3(0.75,0.25,0.25), Reflection_t::Diffuse),	 //Left
	Sphere(1e4,  Vector3(-1e4 + 99, 40.8, 81.6), Vector3(),   Vector3(0.25,0.25,0.75), Reflection_t::Diffuse),	 //Right
	Sphere(1e4,  Vector3(50, 40.8, 1e4),         Vector3(),   Vector3(0.75),           Reflection_t::Diffuse),	 //Back
	Sphere(1e4,  Vector3(50, 40.8, -1e4 + 170),  Vector3(),   Vector3(),               Reflection_t::Diffuse),	 //Front
	Sphere(1e4,  Vector3(50, 1e4, 81.6),         Vector3(),   Vector3(0.75),           Reflection_t::Diffuse),	 //Bottom
	Sphere(1e4,  Vector3(50, -1e4 + 81.6, 81.6), Vector3(),   Vector3(0.75),           Reflection_t::Diffuse),	 //Top
	Sphere(16.5, Vector3(27, 16.5, 47),          Vector3(),   Vector3(0.999),          Reflection_t::Specular),	 //Mirror
	Sphere(16.5, Vector3(73, 16.5, 78),          Vector3(),   Vector3(0.999),          Reflection_t::Refractive),//Glass
	Sphere(600,	 Vector3(50, 681.6 - .27, 81.6), Vector3(12), Vector3(),               Reflection_t::Diffuse)	 //Light

	//Sphere(26, Vec(0, 26 + 18, 0), Vec(12, 12, 12), Vec(), DIFF),       //单Light
	//// // Sphere(5, Vec(20, 0, 0), Vec(0, 0, 0), Vec(0.75, 0.25, 0.25), DIFF),       //单Light
	////降低墙面的半径
	//Sphere(1e4, Vec(-1e4 - 20, 0, 0), Vec(0, 0, 0), Vec(0.75, 0.25, 0.25), DIFF),
	//Sphere(1e4, Vec(1e4 + 20, 0, 0), Vec(0, 0, 0), Vec(0.25, 0.75, 0.75), DIFF),
	//Sphere(1e4, Vec(0,-1e4 - 20, 0), Vec(0, 0, 0), Vec(0.25, 0.75, 0.25), DIFF),
	//Sphere(1e4, Vec(0,1e4 + 20, 0), Vec(0, 0, 0), Vec(0.75, 0.75, 0.75), DIFF),
	//Sphere(1e4, Vec(0, 0, -1e4 - 20), Vec(0, 0, 0), Vec(0.75, 0.75, 0.75), DIFF),
	//Sphere(5, Vec(-10, -15, 0), Vec(0, 0, 0), Vec(1.0, 1.0, 1.0) * 0.999, SPEC),
	//Sphere(8, Vec(10, -11.5, 0), Vec(0, 0, 0), Vec(1.0, 1.0, 1.0) * 0.999, REFR),

};

__device__ inline bool Intersect(const Sphere* dev_spheres,
	std::size_t nb_spheres,
	const Ray& ray,
	size_t& id) {

	bool hit = false;
	for (std::size_t i = 0u; i < nb_spheres; ++i) {
		if (dev_spheres[i].Intersect(ray)) {
			hit = true;
			id = i;
		}
	}

	return hit;
}
//JINGDU 转化优化
//https://codeplay.com/portal/blogs/2015/06/16/sycl-ing-the-smallpt-raytracer.html
//去掉递归 ray-->trace sphere 
__device__ static Vector3 Radiance(const Sphere* dev_spheres,
	std::size_t nb_spheres,
	const Ray& ray,
	curandState* state) {

	Ray r = ray;
	Vector3 L;
	Vector3 F(1.0);

	while (true) {
		std::size_t id; //碰撞的球体
		if (!Intersect(dev_spheres, nb_spheres, r, id)) { //黑色结束
			return L;
		}

		const Sphere& shape = dev_spheres[id];
		//交点
		const Vector3 p = r(r.m_tmax); 
		//交点法向
		const Vector3 n = Normalize(p - shape.m_p); 
		//自发光 * 概率权重
		L += F * shape.m_e;
		//反射光权重
		F *= shape.m_f;
		// Russian roulette
		//深度
		if (4 < r.m_depth) {
			//颜色最大值
			const JINGDU continue_probability = shape.m_f.Max();
			//返回自发光权重叠加
			if (curand_uniform(state) >= continue_probability) {
				return L;
			}
			//增强
			F /= continue_probability;
		}

		// Next path segment
		switch (shape.m_reflection_t) {
			//理想镜面反射 Ray方向改变 反射光受自身影响
		case Reflection_t::Specular: {
			const Vector3 d = IdealSpecularReflect(r.m_d, n);
			r = Ray(p, d, EPSILON_SPHERE, INFINITY, r.m_depth + 1u);
			break;
		}
								   //透明折射
		case Reflection_t::Refractive: {
			JINGDU pr;
			const Vector3 d = IdealSpecularTransmit(r.m_d, n, REFRACTIVE_INDEX_OUT, REFRACTIVE_INDEX_IN, pr, state);
			//折射选择 透射或者反射 一条光路
			F *= pr;
			r = Ray(p, d, EPSILON_SPHERE, INFINITY, r.m_depth + 1u);
			break;
		}
									 //漫反射
		default: {
			const Vector3 w = (0.0 > n.Dot(r.m_d)) ? n : -n;
			const Vector3 u = Normalize((abs(w.x) > 0.1 ? Vector3(0.0, 1.0, 0.0) : Vector3(1.0, 0.0, 0.0)).Cross(w));
			const Vector3 v = w.Cross(u);

			const Vector3 sample_d = CosineWeightedSampleOnHemisphere(curand_uniform(state), curand_uniform(state));
			const Vector3 d = Normalize(sample_d.x * u + sample_d.y * v + sample_d.z * w);
			r = Ray(p, d, EPSILON_SPHERE, INFINITY, r.m_depth + 1u);
		}
		}
	}

}
//CPU 调用GPU执行 有并行参数
__global__ static void kernel(const Sphere* dev_spheres,
	std::size_t nb_spheres,
	std::uint32_t w,
	std::uint32_t h,
	Vector3* Ls,
	std::uint32_t nb_samples) {
	//blockDim threads in Block 
	//how many Grids each Grid Multiple Block 
	//threads in Grid  X direction
	//可以1到3维
	const std::uint32_t x = threadIdx.x + blockIdx.x * blockDim.x;
	const std::uint32_t y = threadIdx.y + blockIdx.y * blockDim.y;
	const std::uint32_t offset = x + y * blockDim.x * gridDim.x;

	if (x >= w || y >= h) {
		return;
	}

	// RNG
	curandState state;
	curand_init(offset, 0u, 0u, &state);

	const Vector3 eye = { 50.0, 52.0, 295.6 };
	const Vector3 gaze = Normalize(Vector3(0.0, -0.042612, -1.0));
	const JINGDU fov = 0.5135;
	const Vector3 cx = { w * fov / h, 0.0, 0.0 };
	const Vector3 cy = Normalize(cx.Cross(gaze)) * fov;

	//x y 像素 sx sy 超采样 2*2  子像素采样
	for (std::size_t sy = 0u, i = (h - 1u - y) * w + x; sy < 2u; ++sy) { // 2 subpixel row

		for (std::size_t sx = 0u; sx < 2u; ++sx) { // 2 subpixel column 

			Vector3 L;

			for (std::size_t s = 0u; s < nb_samples; ++s) { // samples per subpixel
				const JINGDU u1 = 2.0 * curand_uniform(&state);
				const JINGDU u2 = 2.0 * curand_uniform(&state);
				const JINGDU dx = (u1 < 1.0) ? sqrt(u1) - 1.0 : 1.0 - sqrt(2.0 - u1);
				const JINGDU dy = (u2 < 1.0) ? sqrt(u2) - 1.0 : 1.0 - sqrt(2.0 - u2);
				const Vector3 d = cx * (((sx + 0.5 + dx) * 0.5 + x) / w - 0.5) +
					cy * (((sy + 0.5 + dy) * 0.5 + y) / h - 0.5) + gaze;

				L += Radiance(dev_spheres, nb_spheres,
					Ray(eye + d * 130, Normalize(d), EPSILON_SPHERE), &state)
					* (1.0 / nb_samples);
			}

			Ls[i] += 0.25f * Clamp(L);
		}
	}
}

static void Render(std::uint32_t nb_samples) noexcept {
	const std::uint32_t w = 1024u;
	const std::uint32_t h = 768u;
	const std::uint32_t nb_pixels = w * h;

	// Set up device memory
	//HANDLE_ERROR( cudaMemcpyToSymbol(dev_spheres, spheres, sizeof(spheres)) );
	Sphere* dev_spheres;
	HANDLE_ERROR(cudaMalloc((void**)&dev_spheres, sizeof(g_spheres)));
	HANDLE_ERROR(cudaMemcpy(dev_spheres, g_spheres, sizeof(g_spheres), cudaMemcpyHostToDevice));
	//输出每个像素亮度
	Vector3* dev_Ls;
	HANDLE_ERROR(cudaMalloc((void**)&dev_Ls, nb_pixels * sizeof(Vector3)));
	HANDLE_ERROR(cudaMemset(dev_Ls, 0, nb_pixels * sizeof(Vector3)));

	//block 2维 x 每个block 16线程 y  
	// Kernel execution block数量
	const dim3 nblocks(w / 16u, h / 16u);
	//多少个block 多少thread   传入sphere 数量 width height 
	const dim3 nthreads(16u, 16u);
	kernel<<< nblocks, nthreads >>>(dev_spheres, _countof(g_spheres), w, h, dev_Ls, nb_samples);

	// Set up host memory
	Vector3* Ls = (Vector3*)malloc(nb_pixels * sizeof(Vector3));
	// Transfer device -> host
	HANDLE_ERROR(cudaMemcpy(Ls, dev_Ls, nb_pixels * sizeof(Vector3), cudaMemcpyDeviceToHost));

	// Clean up device memory
	HANDLE_ERROR(cudaFree(dev_Ls));
	HANDLE_ERROR(cudaFree(dev_spheres));
	
	//存盘PPM文件
	WritePPM(w, h, Ls);

	// Clean up host memory
	free(Ls);
}
//double 100 36s  float 100 7s
int main(int argc, char* argv[]) {
	auto start = std::chrono::high_resolution_clock::now();
	//const std::uint32_t nb_samples = (2 == argc) ? atoi(argv[1]) / 4 : 1;
	std::uint32_t nb_samples = 1000;
	Render(nb_samples);
	auto end = std::chrono::high_resolution_clock::now();
	auto durat = std::chrono::duration_cast<milliseconds>(end - start);
	std::cout << "Time:" << (durat.count() / 1000.0f) << std::endl;
	return 0;
}
